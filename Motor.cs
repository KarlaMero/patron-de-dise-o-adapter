﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Motores
{
  public abstract class Motor
    {
//la interfaz que conoce el cliente y la que espera que tengan las demas clases
//De la cual van a heredar los metodos comunes
        public abstract void Acelerar();
        public abstract void Arrancar();
        public abstract void Detener();
        public abstract void CargarCombustible();
    }
}

    
