﻿using Motores;
using System;

namespace MOTORES
{
    class Program
    {
        static void Main(string[] args)
            //Representa al cliente que usa los diferentes tipos de motores
        {
            Console.WriteLine("*********Motor Naftero***********");
            MotorNaftero motor1 = new MotorNaftero();
            motor1.Arrancar();
            motor1.Acelerar();
            motor1.Detener();
            motor1.CargarCombustible();

            Console.ReadLine();

            Console.WriteLine("*********Motor Diesel***********");
            MotorDiesel motor2 = new MotorDiesel();
            motor2.Arrancar();
            motor2.Acelerar();
            motor2.Detener();
            motor2.CargarCombustible();

            Console.ReadLine();

            Console.WriteLine("*********Motor Elecrtico***********");//por medio del patron adapter llamamos a los metodos comunes que usa nuestro cliente
            MotorElectricoAdapter motor3 = new MotorElectricoAdapter();
            motor3.Arrancar();
            motor3.Acelerar();
            motor3.Detener();
            motor3.CargarCombustible();

            Console.ReadLine();
        }
    }
}
