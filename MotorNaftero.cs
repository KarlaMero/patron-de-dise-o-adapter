﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Motores
{
   public  class MotorNaftero : Motor //Hereda de la clase motor sus metodos 
   //hereda de l clase motor sus funcionalidades
    {

        public override void Acelerar()//Imprimer en consola lo que hace el metodo
        {
            Console.WriteLine("Acelerando el motor naftero..");
        }

        public override void Arrancar()
        {
            Console.WriteLine("Arrancando el motor naftero..");
        }

        public override void CargarCombustible()
        {
            Console.WriteLine("Cargando combustible al motor naftero..");
        }

        public override void Detener()
        {
            Console.WriteLine("Deteniendo el motor naftero..");
        }
    }
}

