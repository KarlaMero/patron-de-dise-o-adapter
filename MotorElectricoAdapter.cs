﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Motores
{
     class MotorElectricoAdapter:Motor //hereda de la clase Motor los metodos, es el adaptador que nos permitira comunicar ala clase padre con el adaptado
    {
          private MotorElectrico motorElectrico=new MotorElectrico();//Creamos un clase privada 
                                                                     //sobreescrinbimos los metodos de la clase Motor  
        public override void Arrancar()//para poder arrancar ebemos saber si esta conectado y activado 
        {
            motorElectrico.Conectar();
            motorElectrico.Activar();

        }                                                         
        public override void Acelerar()//para acelerar tiene que estar en movimiento
        {
            motorElectrico.Mover();
        }
        public override void Detener() //para detener primero desactivamos y luego va ir parando el motor
        {
            motorElectrico.Desactivar();
            motorElectrico.Parar();
        }

        public override void CargarCombustible() 
        {
            motorElectrico.Enchufar();
        }

       //de esta manera el adaptador sirve como puente para comunicar la clase padre y la nueva clase
    }
}
