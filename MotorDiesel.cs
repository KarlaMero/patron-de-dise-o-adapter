﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Motores
{
   public  class MotorDiesel:Motor //hereda de la clase motores sus metodos
        //va ha ser uso de la funcionalidades que usa el sistema
    {
        public override void Acelerar()
        {
            Console.WriteLine("Acelerando el motor diesel..");//imprimimos en consola lo que hace mi metodo
        }

        public override void Arrancar()
        {
            Console.WriteLine("Arrancando el motor diesel..");
        }

        public override void CargarCombustible()
        {
            Console.WriteLine("Cargando combustible al motor diesel..");
        }

        public override void Detener()
        {
            Console.WriteLine("Deteniendo el motor diesel..");
        }
    }
}

